--------------------------------------------------------------------------------
-- Schema creation script for in-memory database.
--------------------------------------------------------------------------------

drop table if exists game;
drop table if exists authority;
drop table if exists user;
drop table if exists publisher;
drop table if exists game_synonym;

create table user (
    id bigint identity primary key,
    username varchar(50) unique not null,
    password varchar(100) not null,
    enabled boolean not null
);

create table authority (
    id bigint identity primary key,
    id_user bigint not null,
    authority varchar(50) not null,
    constraint fk_authorities_user foreign key(id_user) references user(id)
);
create unique index ix_auth_user on authority(id_user,authority);

create table game (
    id bigint identity primary key,
    name varchar(200) not null,
    alias varchar(200) not null,
    platform varchar(10) not null,
    publisher varchar(100),
    release_date datetime,
    image_url_cover varchar(200),
    score_altogamer int,
    score_metacritic int,
    url_metacritic varchar(200),
    score_destructoid double,
    url_destructoid varchar(200),
    score_gamespot double,
    url_gamespot varchar(200),
    score_gamesradar int,
    url_gamesradar varchar(200),
    score_ign double,
    url_ign varchar(200),
    score_pcgamer int,
    url_pcgamer varchar(200),
    price_steam double,
    url_steam varchar(200),
    price_gamersgate double,
    url_gamersgate varchar(200),
    price_gog double,
    url_gog varchar(200),
    score_videogamer double,
    url_videogamer varchar(200),
    featured boolean default false not null,
    last_updated datetime not null
);

create table game_synonym (
    id bigint identity primary key,
    alias varchar(200) not null,
    synonym varchar(200) not null
);
create unique index ix_synonym on game_synonym(synonym);

create table publisher (
    id bigint identity primary key,
    name varchar(200) not null,
    image_url_cover varchar(200)
);

--
-- Test data.
--

--
-- Password hashed using org.springframework.security.crypto.password.StandardPasswordEncoder.
-- Test users password is always "test".
--
insert into user (id, username, password, enabled) values (1, 'admin', '93747b1d4d1489c76a2c077d71cf46e3a21bab2588127d62f29180d94106da6b8d206396f47ff17d', true);
insert into authority (id_user, authority) values (1, 'admin');

INSERT INTO game (id, name, alias, platform, publisher, release_date, score_metacritic, url_metacritic, score_destructoid, url_destructoid, score_gamespot, url_gamespot, score_ign, url_ign, last_updated, price_steam, url_steam, featured, price_gamersgate, url_gamersgate, image_url_cover, score_altogamer) VALUES
(1, 'Borderlands: Game of the Year', 'borderlands-game-of-the-year', 'PC', NULL, NULL, NULL, NULL, 8.0, NULL, 9.0, NULL, NULL, NULL,  current_date, 29.99, 'http://store.steampowered.com/app/901566/?snr=1_7_7_230_150_64', false, 29.95, 'http://www.gamersgate.com/DD-BLGOTY/borderlands-game-of-the-year', NULL, NULL);
INSERT INTO game (id, name, alias, platform, publisher, release_date, score_metacritic, url_metacritic, score_destructoid, url_destructoid, score_gamespot, url_gamespot, score_ign, url_ign, last_updated, price_steam, url_steam, featured, price_gamersgate, url_gamersgate, image_url_cover, score_altogamer) VALUES
(2, 'Risen Complete Collection ', 'risen-complete-collection', 'PC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, current_date, NULL, NULL, 0, 49.99, 'http://www.gamersgate.com/DDB-RISENCB/risen-complete-collection-bundle', NULL, NULL);
INSERT INTO game (id, name, alias, platform, publisher, release_date, score_metacritic, url_metacritic, score_destructoid, url_destructoid, score_gamespot, url_gamespot, score_ign, url_ign, last_updated, price_steam, url_steam, featured, price_gamersgate, url_gamersgate, image_url_cover, score_altogamer) VALUES
(3, 'Far Cry 3', 'far-cry-3', 'PC', 'Ubisoft', current_date, 91, 'http://www.metacritic.com/game/pc/far-cry-3', 6.0, NULL, 9.0, 'http://www.gamespot.com/reviews/far-cry-3-review/1900-6400897/', 9.0, 'http://www.ign.com/games/far-cry-3/pc-53492', current_date, 29.99, 'http://store.steampowered.com/app/220240/?snr=1_7_7_230_150_34', 0, 19.47, 'http://www.gamersgate.com/DD-FC3/far-cry-3', NULL, 74);
INSERT INTO game (id, name, alias, platform, publisher, release_date, score_metacritic, url_metacritic, score_destructoid, url_destructoid, score_gamespot, url_gamespot, score_ign, url_ign, last_updated, price_steam, url_steam, featured, price_gamersgate, url_gamersgate, image_url_cover, score_altogamer) VALUES
(4, 'BioShock', 'bioshock', 'PC', '2K Games', current_date, 96, 'http://www.metacritic.com/game/pc/bioshock', NULL, NULL, 9.0, 'http://www.gamespot.com/reviews/bioshock-review/1900-6177215/', 9.7, 'http://www.ign.com/games/bioshock/pc-707640', current_date, 19.99, 'http://store.steampowered.com/app/7670/?snr=1_7_7_230_150_96', 0, 19.95, 'http://www.gamersgate.com/DD-BIO/bioshock', NULL, 94);
INSERT INTO game (id, name, alias, platform, publisher, release_date, score_metacritic, url_metacritic, score_destructoid, url_destructoid, score_gamespot, url_gamespot, score_ign, url_ign, last_updated, price_steam, url_steam, featured, price_gamersgate, url_gamersgate, image_url_cover, score_altogamer) VALUES
(5, 'BioShock 2', 'bioshock-2', 'PC', '2K Games', current_date, 88, 'http://www.metacritic.com/game/pc/bioshock-2', NULL, NULL, 8.5, 'http://www.gamespot.com/reviews/bioshock-2-review/1900-6249961/', 9.1, 'http://www.ign.com/games/bioshock-2/pc-14240350', current_date, 19.99, 'http://store.steampowered.com/app/8850/?snr=1_7_7_230_150_74', 0, 19.95, 'http://www.gamersgate.com/DD-BIO2US/bioshock-2', NULL, 88);
INSERT INTO game (id, name, alias, platform, publisher, release_date, score_metacritic, url_metacritic, score_destructoid, url_destructoid, score_gamespot, url_gamespot, score_ign, url_ign, last_updated, price_steam, url_steam, featured, price_gamersgate, url_gamersgate, image_url_cover, score_altogamer) VALUES
(6, 'Borderlands 2', 'borderlands-two', 'PC', NULL, NULL, NULL, NULL, 8.0, NULL, 9.0, NULL, NULL, NULL,  current_date, 29.99, 'http://store.steampowered.com/app/901566/?snr=1_7_7_230_150_64', false, 29.95, 'http://www.gamersgate.com/DD-BLGOTY/borderlands-game-of-the-year', NULL, NULL);


INSERT INTO game_synonym (id, alias, synonym) VALUES
(1, 'borderlands-game-of-the-year', 'borderlands-goty');

INSERT INTO publisher(id, name, image_url_cover) VALUES
(1, '2K Games', '');
INSERT INTO publisher(id, name, image_url_cover) VALUES
(2, 'Valve', 'http://imagen');
