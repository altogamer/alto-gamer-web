/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.controller;

import static org.junit.Assert.*;
import com.altogamer.AbstractDatabaseTest;
import com.altogamer.domain.Game;
import com.altogamer.domain.Platform;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;

public class GameControllerTest extends AbstractDatabaseTest {

    @Autowired
    private GameController instance;

    @Test
    public void showByAliasAndPlatform_idExists_returnsViewOfGameFound() {
        String alias = "far-cry-3";
        ModelMap model = new ModelMap();
        String view = instance.showByAliasAndPlatform(alias, Platform.PC, model);
        Game gameFound = (Game) model.get("game");
        assertNotNull(gameFound);
        assertEquals(alias, gameFound.getAlias());
        assertNotNull(view);
        assertEquals("game", view);
    }

}
