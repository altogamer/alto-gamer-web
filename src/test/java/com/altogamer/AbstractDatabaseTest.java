/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer;

import javax.sql.DataSource;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Abstract base class for tests. It loads the Spring Framework context, and
 * prepares an in-memory database with test data.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:context.xml",
    "classpath:security.xml",
    "classpath:web-crawler.xml",
    "classpath:datasource-test.xml"
})
public abstract class AbstractDatabaseTest {

    @Autowired
    private DataSource dataSource;

    protected JdbcTemplate jdbcTemplate;

    @Before
    public void prepareDatabaseConnection() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @After
    public void logout() {
        SecurityTestUtils.logout();
    }

}
