/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.service;

import com.altogamer.AbstractDatabaseTest;
import com.altogamer.SecurityTestUtils;
import com.altogamer.grc.WebCrawler;
import com.altogamer.grc.impl.destructoid.DestructoidPcAllUrlIterator;
import com.altogamer.grc.impl.gamersgate.GamersgatePcAllUrlIterator;
import com.altogamer.grc.impl.gamespot.GamespotPcAllUrlIterator;
import com.altogamer.grc.impl.ign.IgnPcAllUrlIterator;
import com.altogamer.grc.impl.metacritic.MetacriticPcAllUrlIterator;
import com.altogamer.grc.impl.steam.SteamPcAllUrlIterator;
import com.altogamer.grc.impl.videogamer.VideogamerPcAllUrlIterator;
import java.io.File;
import java.io.IOException;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.jdbc.JdbcTestUtils;

public class GameUpdateServiceTest extends AbstractDatabaseTest {

    @Autowired
    private GameUpdateService instance;
    @Value("${sitemap.dir}")
    private String sitemapDirectory;
    @Autowired
    @Qualifier("webCrawlerMetacriticPc")
    private WebCrawler metacriticWebCrawler;
    @Autowired
    @Qualifier("webCrawlerDestructoidPc")
    private WebCrawler destructoidWebCrawler;
    @Autowired
    @Qualifier("webCrawlerGamespotPc")
    private WebCrawler gamespotWebCrawler;
    @Autowired
    @Qualifier("webCrawlerIgnPc")
    private WebCrawler ignWebCrawler;
    @Autowired
    @Qualifier("webCrawlerSteamPc")
    private WebCrawler steamWebCrawler;
    @Autowired
    @Qualifier("webCrawlerGamersgatePc")
    private WebCrawler gamersgateWebCrawler;
    @Autowired
    @Qualifier("webCrawlerVideogamerPc")
    private WebCrawler videogamerWebCrawler;

    @Before
    public void prepareWebCrawler() {
        MetacriticPcAllUrlIterator metacriticIterator = new MetacriticPcAllUrlIterator();
        metacriticIterator.setLimit(5);
        metacriticWebCrawler.setCorePoolSize(3);
        metacriticWebCrawler.setMaximumPoolSize(3);
        metacriticWebCrawler.setUrlIterator(metacriticIterator);

        GamespotPcAllUrlIterator gamespotIterator = new GamespotPcAllUrlIterator();
        gamespotIterator.setLimit(5);
        gamespotWebCrawler.setCorePoolSize(3);
        gamespotWebCrawler.setMaximumPoolSize(3);
        gamespotWebCrawler.setUrlIterator(gamespotIterator);

        IgnPcAllUrlIterator ignIterator = new IgnPcAllUrlIterator();
        ignIterator.setLimit(5);
        ignWebCrawler.setCorePoolSize(3);
        ignWebCrawler.setMaximumPoolSize(3);
        ignWebCrawler.setUrlIterator(ignIterator);

        DestructoidPcAllUrlIterator destructoidIterator = new DestructoidPcAllUrlIterator();
        destructoidIterator.setLimit(5);
        destructoidWebCrawler.setCorePoolSize(3);
        destructoidWebCrawler.setMaximumPoolSize(3);
        destructoidWebCrawler.setUrlIterator(destructoidIterator);

        SteamPcAllUrlIterator steamIterator = new SteamPcAllUrlIterator();
        steamIterator.setLimit(5);
        steamWebCrawler.setCorePoolSize(3);
        steamWebCrawler.setMaximumPoolSize(3);
        steamWebCrawler.setUrlIterator(steamIterator);

        GamersgatePcAllUrlIterator gamersgateIterator = new GamersgatePcAllUrlIterator();
        gamersgateIterator.setLimit(5);
        gamersgateWebCrawler.setCorePoolSize(3);
        gamersgateWebCrawler.setMaximumPoolSize(3);
        gamersgateWebCrawler.setUrlIterator(gamersgateIterator);
        
        VideogamerPcAllUrlIterator videogamerIterator = new VideogamerPcAllUrlIterator();
        videogamerIterator.setLimit(5);
        videogamerWebCrawler.setCorePoolSize(3);
        videogamerWebCrawler.setMaximumPoolSize(3);
        videogamerWebCrawler.setUrlIterator(gamersgateIterator);
    }

    @Test
    public void update_noGamesInDatabase_putAllGamesInDatabase() {
        int before = JdbcTestUtils.countRowsInTable(jdbcTemplate, "game");
        instance.update();
        int after = JdbcTestUtils.countRowsInTable(jdbcTemplate, "game");
        assertTrue("There should be more than 1000 new games", (after - before) > 1000);
    }

    @Test
    public void createSitemap_hasGames_createsSitemap() throws IOException {
        SecurityTestUtils.loginAdminUser();
        instance.createSitemap();
        File sitemap = new File(sitemapDirectory + "sitemap_index.xml");
        assertTrue(sitemap.exists());
        assertTrue(sitemap.length() > 0);
    }
}
