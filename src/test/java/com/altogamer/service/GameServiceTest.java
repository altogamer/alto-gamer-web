/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.service;

import com.altogamer.AbstractDatabaseTest;
import com.altogamer.SecurityTestUtils;
import com.altogamer.domain.Game;
import com.altogamer.repository.GameRepository;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;

public class GameServiceTest extends AbstractDatabaseTest {

    private static final char NON_BREAKING_WHITESPACE = (char)160;

    @Autowired
    private GameService gameService;
    @Autowired
    private GameRepository gameRepository;

    @Test
    public void createGameAlias_simpleName_returnsAlias() {
        String name = "Rage";
        String alias = gameService.createGameAlias(name);
        assertEquals("rage", alias);
    }

    @Test
    public void createGameAlias_nameWithSpecialChars_returnsAlias() {
        String name = "Call of Duty: Modern Warfare 3";
        String alias = gameService.createGameAlias(name);
        assertEquals("call-of-duty-modern-warfare-3", alias);
    }

    @Test
    public void createGameAlias_nameWithMinus_returnsAlias() {
        String name = "C-RUSH";
        String alias = gameService.createGameAlias(name);
        assertEquals("c-rush", alias);
    }

    @Test
    public void createGameAlias_nameWithMinusAndApostrophe_returnsAlias() {
        String name = "Babel Rising - Sky's The Limit";
        String alias = gameService.createGameAlias(name);
        assertEquals("babel-rising-skys-the-limit", alias);
    }

    @Test
    public void createGameAlias_nameWithMinusNoWhitespace_returnsAlias() {
        String name = "Babel Rising-Sky's The Limit";
        String alias = gameService.createGameAlias(name);
        assertEquals("babel-rising-skys-the-limit", alias);
    }

    @Test
    public void createGameAlias_nameWithManyMinusNoWhitespace_returnsAlias() {
        String name = "Babel Rising--Sky's The Limit";
        String alias = gameService.createGameAlias(name);
        assertEquals("babel-rising-skys-the-limit", alias);
    }

    @Test
    public void createGameAlias_nameWithManyMinusAndWhitespace_returnsAlias() {
        String name = "Babel Rising -- Sky's The Limit";
        String alias = gameService.createGameAlias(name);
        assertEquals("babel-rising-skys-the-limit", alias);
    }

    @Test
    public void createGameAlias_nameEndingWithSpecialChar_returnsAlias() {
        String name = "Back to the Future: The Game - Episode II: Get Tannen!";
        String alias = gameService.createGameAlias(name);
        assertEquals("back-to-the-future-the-game-episode-ii-get-tannen", alias);
    }

    @Test
    public void createGameAlias_nameWithAmpersandAndWhitespace_returnsAlias() {
        String name = "Grand Theft Auto IV: The Lost & Damned ";
        String alias = gameService.createGameAlias(name);
        assertEquals("grand-theft-auto-iv-the-lost-and-damned", alias);
    }


    @Test
    public void createGameAlias_nameWithAmpersand_returnsAlias() {
        String name = "Grand Theft Auto IV: The Lost&Damned";
        String alias = gameService.createGameAlias(name);
        assertEquals("grand-theft-auto-iv-the-lost-and-damned", alias);
    }

    @Test
    public void createGameAlias_nameWithNonBreakingWhitespaceAtEnd_returnsAlias() {
        String name = "Grand Theft Auto IV: The Lost & Damned" + NON_BREAKING_WHITESPACE;
        String alias = gameService.createGameAlias(name);
        assertEquals("grand-theft-auto-iv-the-lost-and-damned", alias);
    }


    @Test
    public void createGameAlias_nameWithNonBreakingWhitespace_returnsAlias() {
        String name = "Grand" + NON_BREAKING_WHITESPACE + NON_BREAKING_WHITESPACE + "Theft" + NON_BREAKING_WHITESPACE + "Auto IV:" + NON_BREAKING_WHITESPACE + "The Lost & Damned";
        String alias = gameService.createGameAlias(name);
        assertEquals("grand-theft-auto-iv-the-lost-and-damned", alias);
    }


    @Test
    public void findNewReleases_firstPage_returnsGames() {
        List<Game> games = gameService.findNewReleases(0);
        assertNotNull(games);
        assertTrue(games.size() > 0);
    }

    @Test
    public void findNewReleases_emptyPage_returnsEmptyList() {
        List<Game> games = gameService.findNewReleases(99999);
        assertNotNull(games);
        assertTrue(games.isEmpty());
    }


    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void changeFeatured_userNotLoggedIn_throwsSecurityException() {
        gameService.changeFeatured(2L);
    }

    @Test
    public void changeFeatured_gameNotFeatured_returnsGameFeatured() {
        SecurityTestUtils.loginAdminUser();
        Long id = 2L;
        Game game = gameService.changeFeatured(id);
        assertTrue(game.isFeatured());
    }

    @Test
    public void findByFeatured_returnsGamesFeatured() {
        List<Game> games = gameService.findByFeatured();
        assertNotNull(games);
        assertEquals(1, games.size());
    }
}
