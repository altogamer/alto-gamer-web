/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.processor;

import com.altogamer.AbstractDatabaseTest;
import com.altogamer.domain.Game;
import com.altogamer.grc.GameScore;
import com.altogamer.grc.Processor;
import com.altogamer.grc.Url;
import com.altogamer.repository.GameRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.jdbc.JdbcTestUtils;

public class RefreshGameProcessorTest extends AbstractDatabaseTest {

    @Autowired
    private Processor processor;
    @Autowired
    private GameRepository gameRepository;

    private static final char NON_BREAKING_WHITESPACE = (char)160;

    @Test
    public void process_gameWithSpecialName_prepareNameAndInsertGamesInDatabase() {
        List<GameScore> gameScoreList = new ArrayList<>();
        GameScore gameScore;
        gameScore = new GameScore();
        gameScore.setName(NON_BREAKING_WHITESPACE + "Outlast" + NON_BREAKING_WHITESPACE);
        gameScore.setReleaseDate(new Date());
        gameScore.setScore("99");
        gameScore.setUrl("www.metacritic.com/outlast");
        gameScoreList.add(gameScore);
        gameScore = new GameScore();
        gameScore.setName("Metro" + NON_BREAKING_WHITESPACE + "2033");
        gameScore.setReleaseDate(new Date());
        gameScore.setScore("99");
        gameScore.setUrl("www.metacritic.com/metro2033");
        gameScoreList.add(gameScore);

        int before = JdbcTestUtils.countRowsInTable(jdbcTemplate, "game");
        processor.process(gameScoreList, new Url(Url.Platform.PC, Url.Website.METACRITIC, "http://www.metacritic.com/dummy-url"));
        int after = JdbcTestUtils.countRowsInTable(jdbcTemplate, "game");

        assertEquals("The games should be inserted", 2, after - before);

        Game outlast = gameRepository.findByKeywords("Outlast").get(0);
        Game metro = gameRepository.findByKeywords("Metro").get(0);
        assertNull("Alto Score should be null", outlast.getScoreAltogamer());
        assertNull("Alto Score should be null", metro.getScoreAltogamer());
        assertEquals("Outlast", outlast.getName());
        assertEquals("Metro 2033", metro.getName());
    }

    @Test
    public void process_allGamesAreNew_insertGamesInDatabase() {
        List<GameScore> gameScoreList = new ArrayList<>();
        GameScore gameScore;
        gameScore = new GameScore();
        gameScore.setName("Interstellar Marines");
        gameScore.setReleaseDate(new Date());
        gameScore.setScore("99");
        gameScore.setUrl("www.metacritic.com/interstellarmarines");
        gameScoreList.add(gameScore);
        gameScore = new GameScore();
        gameScore.setName("Loadout");
        gameScore.setReleaseDate(new Date());
        gameScore.setScore("99");
        gameScore.setUrl("www.metacritic.com/loadout");
        gameScoreList.add(gameScore);

        int before = JdbcTestUtils.countRowsInTable(jdbcTemplate, "game");
        processor.process(gameScoreList, new Url(Url.Platform.PC, Url.Website.METACRITIC, "http://www.metacritic.com/dummy-url"));
        int after = JdbcTestUtils.countRowsInTable(jdbcTemplate, "game");

        assertEquals("The games should be inserted", 2, after - before);

        Game outlast = gameRepository.findByKeywords("Interstellar Marines").get(0);
        Game loadout = gameRepository.findByKeywords("Loadout").get(0);
        assertNull("Alto Score should be null", outlast.getScoreAltogamer());
        assertNull("Alto Score should be null", loadout.getScoreAltogamer());
    }

    @Test
    public void process_allGamesAlreadyExist_updateGamesInDatabase() {
        List<GameScore> gameScoreList = new ArrayList<>();
        GameScore gameScore;
        gameScore = new GameScore();
        gameScore.setName("BioShock 2");
        gameScore.setReleaseDate(new Date());
        gameScore.setScore("80");
        gameScore.setUrl("www.metacritic.com/bioshock-2");
        gameScoreList.add(gameScore);
        gameScore = new GameScore();
        gameScore.setName("Far Cry 3");
        gameScore.setReleaseDate(new Date());
        gameScore.setScore("90");
        gameScore.setUrl("www.metacritic.com/far-cry-3");
        gameScoreList.add(gameScore);

        int before = JdbcTestUtils.countRowsInTable(jdbcTemplate, "game");
        processor.process(gameScoreList, new Url(Url.Platform.PC, Url.Website.METACRITIC, "http://www.metacritic.com/dummy-url"));
        int after = JdbcTestUtils.countRowsInTable(jdbcTemplate, "game");

        assertEquals("No new games should be inserted", 0, after - before);

        Game bioshock = gameRepository.findOne(5L);
        Game farcry = gameRepository.findOne(3L);
        assertEquals("Alto Score should be set", 85, bioshock.getScoreAltogamer().longValue());
        assertEquals("Alto Score should be set", 83, farcry.getScoreAltogamer().longValue());
    }

    @Test
    public void process_gameWithSynonym_updateGame() {
        List<GameScore> gameScoreList = new ArrayList<>();
        GameScore gameScore;
        gameScore = new GameScore();
        gameScore.setName("Borderlands: GOTY");
        gameScore.setReleaseDate(new Date());
        gameScore.setScore("10.0");
        gameScore.setUrl("www.ign.com/borderlands-goty");
        gameScoreList.add(gameScore);

        int before = JdbcTestUtils.countRowsInTable(jdbcTemplate, "game");
        processor.process(gameScoreList, new Url(Url.Platform.PC, Url.Website.IGN, "http://www.ign.com/dummy-url"));
        int after = JdbcTestUtils.countRowsInTable(jdbcTemplate, "game");

        assertEquals("No new games should be inserted", 0, after - before);

        Game borderlands = gameRepository.findOne(1L);
        assertEquals(10, borderlands.getScoreIgn().longValue());
    }

    @Test
    public void process_gamesWithSamePublisher_addOnePublisherInDatabase() {
        List<GameScore> gameScoreList = new ArrayList<>();
        GameScore gameScore;
        gameScore = new GameScore();
        gameScore.setName("Borderlands");
        gameScore.setReleaseDate(new Date());
        gameScore.setScore("81");
        gameScore.setUrl("www.metacritic.com/borderlands");
        gameScore.setPublisher("2K games");
        gameScoreList.add(gameScore);
        gameScore = new GameScore();
        gameScore.setName("FarCry 3");
        gameScore.setReleaseDate(new Date());
        gameScore.setScore("88");
        gameScore.setUrl("www.metacritic.com/farcry-3");
        gameScore.setPublisher("2K games");
        gameScoreList.add(gameScore);

        int before = JdbcTestUtils.countRowsInTable(jdbcTemplate, "publisher");
        processor.process(gameScoreList, new Url(Url.Platform.PC, Url.Website.METACRITIC, "http://www.metacritic.com/dummy-url"));
        int after = JdbcTestUtils.countRowsInTable(jdbcTemplate, "publisher");

        assertEquals(before + 1, after);
    }
}
