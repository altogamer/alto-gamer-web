/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.repository;

import com.altogamer.AbstractDatabaseTest;
import com.altogamer.domain.Game;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

public class GameRepositoryTest extends AbstractDatabaseTest {

    @Autowired
    private GameRepository gameRepository;

    @Test
    public void findByKeywords_queryMatchesStart_returnsGames() {
        List<Game> games = gameRepository.findByKeywords("Far");
        assertNotNull(games);
        assertTrue(games.size() > 0);
    }

    @Test
    public void findByNameContainingIgnoreCase_queryMatchesEnd_returnsGames() {
        List<Game> games = gameRepository.findByKeywords("land");
        assertNotNull(games);
        assertTrue(games.size() > 0);
    }

    @Test
    public void findByNameContainingIgnoreCase_queryMatchesMiddle_returnsGames() {
        List<Game> games = gameRepository.findByKeywords("rderlan");
        assertNotNull(games);
        assertTrue(games.size() > 0);
    }

}
