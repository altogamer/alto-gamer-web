/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.service.impl;

import com.altogamer.domain.Game;
import com.altogamer.domain.GameSynonym;
import com.altogamer.domain.Platform;
import com.altogamer.repository.GameRepository;
import com.altogamer.repository.GameSynonymRepository;
import com.altogamer.service.*;
import com.altogamer.vo.GameVo;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import jodd.util.StringUtil;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class GameServiceImpl implements GameService {

    private static final int PAGE_SIZE = 12;

    @Autowired
    private GameRepository gameRepository;

    @Override
    public Game getByAliasAndPlatform(String alias, Platform platform) {
        return gameRepository.getByAliasAndPlatform(alias, platform);
    }

     @Override
    public String createGameAlias(String name) {
        return name
                .replaceAll("\\xA0", " ")               // AO (ascii 160) is a Non-breaking space that is sometimes used. Convert it to regular whitespace.
                .trim()                                 //" The Game - Episode II: Get & Tannen! " --> "The Game - Episode II: Get & Tannen!"
                .toLowerCase()                          //"The Game - Episode II: Get & Tannen!"   --> "the game - episode ii: get & tannen!"
                .replaceAll("\\s*[-]+\\s*", "-")        //"the game - episode ii: get & tannen!"   --> "the game-episode ii: get & tannen!"
                .replaceAll("\\s*&\\s*", " and ")       //"the game - episode ii: get & tannen!"   --> "the game-episode ii: get and tannen!"
                .replaceAll("[^\\dA-Za-z -]", "")       //"the game-episode ii: get and tannen!"   --> "the game-episode ii get and tannen"
                .replaceAll("\\s+", "-");               //"the game-episode ii get and tannen"     --> "the-game-episode-ii-get-and-tannen"
    }

    @Override
    public List<Game> findByKeywords(String keywords) {
        return gameRepository.findByKeywords(keywords);
    }

    @Override
    public List<Game> findNewReleases(int page) {
        PageRequest pageRequest = new PageRequest(page, PAGE_SIZE, new Sort(
                new Order(Direction.DESC, "releaseDate"),
                new Order(Direction.ASC, "name")));
        return gameRepository.findByScoreNotNullAndReleaseDateLessThan(tomorrow(), pageRequest);
    }

    private Date tomorrow() {
        GregorianCalendar gc = new GregorianCalendar();
        gc.add(Calendar.DAY_OF_YEAR, 1);
        return gc.getTime();
    }

    @PreAuthorize("hasRole('admin')")
    @Override
    public Game changeFeatured(Long id) {
        Game game = gameRepository.findOne(id);
        if (!game.isFeatured()) {
            //only 1 featured game for now
            gameRepository.updateAllToFeaturedFalse();
        }
        game.setFeatured(!game.isFeatured());
        return game;
    }

    @Override
    public List<Game> findByFeatured() {
        return gameRepository.findByFeaturedTrue();
    }

    @PreAuthorize("hasRole('admin')")
    @Override
    public void update(GameVo gameVo) {
        Game game = gameRepository.findOne(gameVo.getId());
        String imageUrlCover = gameVo.getImageUrlCover();
        if (StringUtil.isBlank(imageUrlCover)) {
            imageUrlCover = null;
        }
        game.setImageUrlCover(imageUrlCover);
    }

    @Override
    public List<Game> findByPublisher(String publisher, int page) {
        PageRequest pageRequest = new PageRequest(page, PAGE_SIZE, new Sort(
                new Order(Direction.DESC, "releaseDate"),
                new Order(Direction.ASC, "name")));
        return gameRepository.findByPublisher(publisher, pageRequest);
    }

    @Override
    public List<Game> findTopGames(int page) {
        PageRequest pageRequest = new PageRequest(page, PAGE_SIZE, new Sort(
                new Order(Direction.DESC, "scoreAltogamer"),
                new Order(Direction.DESC, "releaseDate"),
                new Order(Direction.ASC, "name")));
        return gameRepository.findAll(pageRequest).getContent();
    }

    @Override
    public List<Game> findTopGamesByYear(int year, int page) {
        PageRequest pageRequest = new PageRequest(page, PAGE_SIZE, new Sort(
                new Order(Direction.DESC, "scoreAltogamer"),
                new Order(Direction.DESC, "releaseDate"),
                new Order(Direction.ASC, "name")));

        LocalDateTime since = new LocalDateTime(year, 1, 1, 0, 0, 0, 0);
        LocalDateTime to = new LocalDateTime(year, 12, 31, 23, 59, 59, 999);

        return gameRepository.findByReleaseDate(since.toDate(), to.toDate(), pageRequest);
    }

    @Override
    public Game findById(long id) {
        return gameRepository.findOne(id);
    }
}
