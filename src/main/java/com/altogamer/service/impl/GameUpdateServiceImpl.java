/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.service.impl;

import com.altogamer.domain.Game;
import com.altogamer.grc.WebCrawler;
import com.altogamer.repository.GameRepository;
import com.altogamer.service.GameUpdateService;
import com.redfin.sitemapgenerator.W3CDateFormat;
import com.redfin.sitemapgenerator.W3CDateFormat.Pattern;
import com.redfin.sitemapgenerator.WebSitemapGenerator;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.TimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StopWatch;

@Service
public class GameUpdateServiceImpl implements GameUpdateService {

    private static final Logger logger = LoggerFactory.getLogger(WebCrawler.class);
    private static final String BASE_URL = "http://www.altogamer.com";

    @Autowired
    private GameRepository gameRepository;
    @Value("${sitemap.dir}")
    private String sitemapDirectory;
    @Value("${sitemap.maxUrls}")
    private int sitemapMaxUrls;
    @Value("${grc.maximumPoolSize}")
    private int maximumPoolSize;
    @Value("${grc.corePoolSize}")
    private int corePoolSize;
    @Autowired
    private List<WebCrawler> webCrawlers;

    @Scheduled(cron = "0 0 3 * * ?")
    @Override
    public void update() {
        try {
            StopWatch sw = new StopWatch();
            for (WebCrawler webCrawler : webCrawlers) {
                String taskName = webCrawler.getCollector().getClass().getSimpleName();
                taskName += "/" + webCrawler.getUrlIterator().getClass().getSimpleName();
                logger.info("Starting update with crawler " + taskName);
                sw.start(taskName);
                webCrawler.setCorePoolSize(corePoolSize);
                webCrawler.setMaximumPoolSize(maximumPoolSize);
                webCrawler.run();
                sw.stop();
                logger.info("Finishing update with crawler {} in {}ms", taskName, sw.getLastTaskTimeMillis());
            }
            logger.info("Update to games database completed in {}ms.", sw.getTotalTimeMillis());

            createSitemap();
        } catch (IOException | InterruptedException ex) {
            logger.error("Error while updating games database.", ex);
        }
    }

    @PreAuthorize("hasRole('admin')")
    @Transactional(readOnly = true)
    @Override
    public void createSitemap() throws IOException {
        logger.info("Starting sitemap generation.");
        int pageNumber = 0;
        Page<Game> page;

        File outputDir = new File(sitemapDirectory);
        outputDir.mkdirs();

        W3CDateFormat dateFormat = new W3CDateFormat(Pattern.DAY);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        WebSitemapGenerator wsg = WebSitemapGenerator.builder(BASE_URL, outputDir)
                .dateFormat(dateFormat)
                .maxUrls(sitemapMaxUrls)
                .build();
        wsg.addUrl(BASE_URL + "/");
        do {
            page = gameRepository.findAll(new PageRequest(pageNumber, 300, Direction.ASC, "alias"));
            List<Game> games = page.getContent();
            for (Game game : games) {
                wsg.addUrl(BASE_URL + "/juegos/" + game.getPlatform() + "/" + game.getAlias());
            }
            pageNumber++;
        } while (page.hasNext());

        wsg.write();
        wsg.writeSitemapsWithIndex();
        logger.info("Finished sitemap generation.");
    }

}
