/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.service.impl;

import com.altogamer.domain.Publisher;
import com.altogamer.repository.PublisherRepository;
import com.altogamer.service.*;
import com.altogamer.vo.PublisherVo;
import java.util.List;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PublisherServiceImpl implements PublisherService {

    private static final int PAGE_SIZE = 12;

    @Autowired
    private PublisherRepository publisherRepository;

    @Override
    public void save(String publisherName) {
        if (publisherName != null) {
            Publisher publisher = publisherRepository.findByName(publisherName);
            if (publisher == null) {
                publisher = new Publisher();
                publisher.setName(publisherName);
                publisherRepository.save(publisher);
            }
        }
    }

    @Override
    public List<Publisher> findAll(int page) {
        PageRequest pageRequest = new PageRequest(page, PAGE_SIZE,
                new Sort(new Order(Direction.ASC, "name")));
        return publisherRepository.findAll(pageRequest).getContent();
    }

    @PreAuthorize("hasRole('admin')")
    @Override
    public void update(PublisherVo publisherVo) {
        Publisher publisher = publisherRepository.findOne(publisherVo.getId());
        String imageUrlCover = publisherVo.getImageUrlCover();
        if (StringUtil.isBlank(imageUrlCover)) {
            imageUrlCover = null;
        }
        publisher.setImageUrlCover(imageUrlCover);
    }
}
