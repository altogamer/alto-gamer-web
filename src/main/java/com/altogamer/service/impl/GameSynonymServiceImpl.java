/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.service.impl;

import com.altogamer.domain.Game;
import com.altogamer.domain.GameSynonym;
import com.altogamer.repository.GameRepository;
import com.altogamer.repository.GameSynonymRepository;
import com.altogamer.service.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class GameSynonymServiceImpl implements GameSynonymService {

    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private GameSynonymRepository gameSynonymRepository;

    @PreAuthorize("hasRole('admin')")
    @Override
    public GameSynonym addSynonym(GameSynonym gameSynonym) {
        Game game = gameRepository.findByAlias(gameSynonym.getSynonym());
        gameRepository.delete(game);
        return gameSynonymRepository.save(gameSynonym);
    }

    @Override
    public GameSynonym findBySynonym(String synonym) {
        return gameSynonymRepository.findBySynonym(synonym);
    }

    @PreAuthorize("hasRole('admin')")
    @Override
    public List<GameSynonym> findByAlias(String alias) {
        return gameSynonymRepository.findByAlias(alias);
    }

    @PreAuthorize("hasRole('admin')")
    @Override
    public void delete(long id) {
        gameSynonymRepository.delete(id);
    }

}
