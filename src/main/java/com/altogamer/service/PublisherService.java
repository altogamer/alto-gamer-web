/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.service;

import com.altogamer.domain.Publisher;
import com.altogamer.vo.PublisherVo;
import java.util.List;

/**
 * Service for Publisher.
 */
public interface PublisherService {

    void save(String publisherName);
    List<Publisher> findAll(int page);
    void update(PublisherVo publisherVo);

}
