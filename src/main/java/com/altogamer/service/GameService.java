/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.service;

import com.altogamer.domain.Game;
import com.altogamer.domain.Platform;
import com.altogamer.vo.GameVo;
import java.util.List;

/**
 * Service for Games.
 */
public interface GameService {

    Game getByAliasAndPlatform(String alias, Platform platform);

    /** Creates a string suitable for use in an URL from the given game's name. */
    String createGameAlias(String name);

    Game findById(long id);

    List<Game> findByKeywords(String keywords);

    List<Game> findNewReleases(int page);

    /** Change highlight of a game, true to false or false to true **/
    Game changeFeatured(Long id);

    List<Game> findByFeatured();

    void update(GameVo game);

    List<Game> findByPublisher(String publisher, int page);

    List<Game> findTopGames(int page);

    List<Game> findTopGamesByYear(int year, int page);
}
