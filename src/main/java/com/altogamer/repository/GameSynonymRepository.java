/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. *//* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.repository;

import org.springframework.data.repository.CrudRepository;
import com.altogamer.domain.GameSynonym;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Data repository for Game Synonym domain object.
 */
public interface GameSynonymRepository extends CrudRepository<GameSynonym, Long>, JpaRepository<GameSynonym, Long> {
    
    GameSynonym findBySynonym(String synonym);
    List<GameSynonym> findByAlias(String alias);
}
