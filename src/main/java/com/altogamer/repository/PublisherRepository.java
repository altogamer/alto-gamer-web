/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. *//* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.repository;

import org.springframework.data.repository.CrudRepository;
import com.altogamer.domain.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Data repository for Publisher domain object.
 */
public interface PublisherRepository extends CrudRepository<Publisher, Long>, JpaRepository<Publisher, Long> {

    Publisher findByName(String publisherName);

}
