/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.controller;

import com.altogamer.domain.Game;
import com.altogamer.domain.GameSynonym;
import com.altogamer.service.GameService;
import com.altogamer.service.GameSynonymService;
import com.altogamer.service.PublisherService;
import com.altogamer.vo.GameVo;
import com.altogamer.vo.PublisherVo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class AdminRestController {

    @Autowired
    private GameService gameService;
    @Autowired
    private GameSynonymService gameSynonymService;
    @Autowired
    private PublisherService publisherService;

    @RequestMapping("/api/game-change-featured")
    public Game changeGameFeatured(@RequestParam Long id) {
        return gameService.changeFeatured(id);
    }

    @RequestMapping("/api/games/{id}")
    public Game findGameById(@PathVariable Long id) {
        return gameService.findById(id);
    }

    @RequestMapping(value = "/api/games", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateGame(@RequestBody GameVo gameVo) {
        gameService.update(gameVo);
    }

    @RequestMapping("/api/synonym/alias/{alias}")
    public List<GameSynonym> findGameSynonymByAlias(@PathVariable String alias) {
        return gameSynonymService.findByAlias(alias);
    }

    @RequestMapping(value = "/api/synonym", method = RequestMethod.POST)
    public GameSynonym addSynonym(@RequestBody GameSynonym gameSynonym) {
        return gameSynonymService.addSynonym(gameSynonym);
    }

    @RequestMapping(value = "/api/synonym/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteSynonym(@PathVariable Long id) {
        gameSynonymService.delete(id);
    }


    @RequestMapping(value = "/api/publisher", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updatePublisher(@RequestBody PublisherVo publisherVo) {
        publisherService.update(publisherVo);
    }

}
