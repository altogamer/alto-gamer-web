/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.controller;

import com.altogamer.domain.Game;
import com.altogamer.service.GameService;
import com.altogamer.vo.PageVo;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GameRestController {
    @Autowired
    private GameService gameService;

    @RequestMapping("/api/games/all")
    public PageVo<Game> findNewReleases(@RequestParam int page, HttpServletRequest request) {
        String url = request.getContextPath() + "/api/games/all?page=" + (page + 1);
        PageVo<Game> pageVo = new PageVo<>();
        pageVo.setItems(gameService.findNewReleases(page));
        pageVo.setNextPageUrl(url);
        return pageVo;
    }

    @RequestMapping("/api/search/{keywords}")
    public List<Game> findByKeywords(@PathVariable String keywords) {
        return gameService.findByKeywords(keywords);
    }

    @RequestMapping("/api/ranking")
    public PageVo<Game> findRanking(@RequestParam int page, HttpServletRequest request) {
        String nextPageUrl = request.getContextPath() + "/api/ranking?page=" + (page +1);
        PageVo<Game> pageVo = new PageVo<>();
        pageVo.setItems(gameService.findTopGames(page));
        pageVo.setNextPageUrl(nextPageUrl);
        return pageVo;
    }

    @RequestMapping("/api/ranking/{year}")
    public PageVo<Game> findRankingByYear(@RequestParam int page, @PathVariable int year, HttpServletRequest request) {
        String nextPageUrl = request.getContextPath() + "/api/ranking/" + year + "?page=" + (page +1);
        PageVo<Game> pageVo = new PageVo<>();
        pageVo.setItems(gameService.findTopGamesByYear(year, page));
        pageVo.setNextPageUrl(nextPageUrl);
        return pageVo;
    }

    @RequestMapping("/api/publishers/{publisher}")
    public PageVo<Game> findByPublisher(@PathVariable String publisher, @RequestParam int page, HttpServletRequest request) {
        String nextPageUrl = request.getContextPath() + "/api/publishers/" + publisher + "?page=" + (page +1);
        PageVo<Game> pageVo = new PageVo<>();
        pageVo.setItems(gameService.findByPublisher(publisher, page));
        pageVo.setNextPageUrl(nextPageUrl);
        return pageVo;
    }
}
