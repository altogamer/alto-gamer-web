/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PublisherController {

    @RequestMapping("/distribuidoras")
    public String showPublishers(ModelMap model, HttpServletRequest request) {
        String url = request.getContextPath() + "/api/publishers?page=0";
        model.addAttribute("sectionTitle1", "Distribuidoras");
        model.addAttribute("sectionTitle2", "de videojuegos");
        model.addAttribute("url", url);
        return "publishers";
    }

    @RequestMapping("/distribuidoras/{publisher}")
    public String showByPublisher(@PathVariable String publisher, ModelMap model, HttpServletRequest request) {
        String url = request.getContextPath() + "/api/publishers/" + publisher + "?page=0";
        model.addAttribute("sectionTitle1", publisher);
        model.addAttribute("sectionTitle2", "últimos lanzamientos");
        model.addAttribute("url", url);
        return "publisher";
    }
}
