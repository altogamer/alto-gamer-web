/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.controller;

import com.altogamer.domain.Game;
import com.altogamer.domain.Platform;
import com.altogamer.exception.ResourceNotFoundException;
import com.altogamer.service.GameService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * A controller for games.
 */
@Controller
public class GameController {

    @Autowired
    private GameService gameService;

    @RequestMapping("/")
    public String showHomepage(ModelMap model, HttpServletRequest request) {
        String url = request.getContextPath() + "/api/games/all?page=0";
        model.addAttribute("featured", gameService.findByFeatured());
        model.addAttribute("url", url);
        return "home";
    }

    @RequestMapping("/juegos/{platform}/{alias}")
    public String showByAliasAndPlatform(@PathVariable String alias, @PathVariable Platform platform, ModelMap model) {
        Game game = gameService.getByAliasAndPlatform(alias, platform);
        if (game != null) {
            model.addAttribute("game", game);
            return "game";
        } else {
            throw new ResourceNotFoundException();
        }
    }

    @RequestMapping("/ranking")
    public String showRanking(ModelMap model, HttpServletRequest request) {
        String url = request.getContextPath() + "/api/ranking?page=0";
        model.addAttribute("sectionTitle1", "Alto Ranking");
        model.addAttribute("sectionTitle2", "Los mejores juegos de todos los tiempos");
        model.addAttribute("url", url);
        return "ranking";
    }

    @RequestMapping("/ranking/{year}")
    public String showRankingByYear(@PathVariable int year, ModelMap model, HttpServletRequest request) {
        String url = request.getContextPath() + "/api/ranking/" + year + "?page=0";
        model.addAttribute("sectionTitle1", year);
        model.addAttribute("sectionTitle2", "Los mejores juegos del año " + year);
        model.addAttribute("url", url);
        return "ranking";
    }

}
