/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.controller;

import com.altogamer.service.GameUpdateService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private GameUpdateService gameUpdateService;

    @RequestMapping("/login")
    public String showLogin() {
        return "login";
    }

    @PreAuthorize("hasRole('admin')")
    @RequestMapping("/manager")
    public String showManager() throws IOException {
        return "manager";
    }

    @PreAuthorize("hasRole('admin')")
    @RequestMapping("/game-update")
    public void update(HttpServletResponse response) throws IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        writer.write("Starting games database update...\n");
        response.flushBuffer();
        gameUpdateService.update();
        writer.write("Finished.");
    }

    @RequestMapping("/sitemap-update")
    public void createSitemap(HttpServletResponse response) throws IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        writer.write("Creating sitemap...\n");
        response.flushBuffer();
        gameUpdateService.createSitemap();
        writer.write("Finished.");
    }

}
