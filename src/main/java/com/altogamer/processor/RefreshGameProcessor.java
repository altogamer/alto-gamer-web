/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.processor;

import com.altogamer.domain.Game;
import com.altogamer.domain.GameSynonym;
import com.altogamer.domain.Platform;
import com.altogamer.grc.GameScore;
import com.altogamer.grc.Processor;
import com.altogamer.grc.Url;
import com.altogamer.repository.GameRepository;
import com.altogamer.service.GameService;
import com.altogamer.service.GameSynonymService;
import com.altogamer.service.PublisherService;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Processor for updating games information.
 */
@Component
@Transactional
public class RefreshGameProcessor implements Processor {

    private static final Logger logger = LoggerFactory.getLogger(RefreshGameProcessor.class);

    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private GameService gameService;
    @Autowired
    private GameSynonymService gameSynonymService;
    @Autowired
    private PublisherService publisherService;

    @Override
    public void process(List<GameScore> games, Url url) {
        Date now = new Date();
        for (GameScore gameScore : games) {
            Platform platform = Platform.valueOf(url.getPlatform().toString());
            String alias = gameService.createGameAlias(gameScore.getName());
            GameSynonym gameSynonym = gameSynonymService.findBySynonym(alias);
            if (gameSynonym != null) {
                alias = gameSynonym.getAlias();
            }
            Game game = gameRepository.getByAliasAndPlatform(alias, platform);
            if (game == null) {
                //insert new game
                game = new Game();
                game.setName(prepareName(gameScore.getName()));
                game.setAlias(alias);
                game.setPlatform(platform);
                game.setFeatured(false);
                loadGameFromGameScore(game, gameScore, now, url);
                publisherService.save(game.getPublisher());
                gameRepository.save(game);
            } else {
                //update existing game
                loadGameFromGameScore(game, gameScore, now, url);
            }
        }
    }

    private String prepareName(String name) {
        // AO (ascii 160) is a Non-breaking space that is sometimes used. Convert it to regular whitespace.
        return name.replaceAll("\\xA0", " ").trim();
    }

    private void loadGameFromGameScore(Game game, GameScore gameScore, Date updateDate, Url url) {
        game.setLastUpdated(updateDate);
        switch (url.getWebsite()) {
            case METACRITIC:
                game.setReleaseDate(gameScore.getReleaseDate());
                game.setPublisher(gameScore.getPublisher());
                game.setScoreMetacritic(parseScoreAsInteger(gameScore.getScore()));
                game.setUrlMetacritic(gameScore.getUrl());
                break;
            case DESTRUCTOID:
                game.setScoreDestructoid(parseScoreAsDouble(gameScore.getScore()));
                game.setUrlDestructoid(gameScore.getUrl());
                break;
            case GAMESPOT:
                game.setScoreGamespot(parseScoreAsDouble(gameScore.getScore()));
                game.setUrlGamespot(gameScore.getUrl());
                break;
            case GAMESRADAR:
                game.setScoreGamesradar(parseScoreAsDouble(gameScore.getScore()));
                game.setUrlGamesradar(gameScore.getUrl());
                break;
            case IGN:
                game.setScoreIgn(parseScoreAsDouble(gameScore.getScore()));
                game.setUrlIgn(gameScore.getUrl());
                break;
            case PCGAMER:
                game.setScorePcgamer(parseScoreAsInteger(gameScore.getScore()));
                game.setUrlPcgamer(gameScore.getUrl());
                break;
            case STEAM:
                game.setPriceSteam(parseScoreAsDouble(gameScore.getPrice()));
                game.setUrlSteam(gameScore.getUrl());
                break;
            case GAMERSGATE:
                game.setPriceGamersgate(parseScoreAsDouble(gameScore.getPrice()));
                game.setUrlGamersgate(gameScore.getUrl());
                break;
            case GOG:
                game.setPriceGog(parseScoreAsDouble(gameScore.getPrice()));
                game.setUrlGog(gameScore.getUrl());
                break;
            case VIDEOGAMER:
                game.setScoreVideogamer(parseScoreAsDouble(gameScore.getScore()));
                game.setUrlVideogamer(gameScore.getUrl());
                break;
            default:
                logger.warn("Unknown review website for game: " + url.getWebsite());
        }

        setAltogamerScore(game);
    }

    private Integer parseScoreAsInteger(String score) {
        try {
            return Integer.parseInt(score);
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    private Double parseScoreAsDouble(String score) {
        try {
            return Double.parseDouble(score);
        } catch (NumberFormatException | NullPointerException ex) {
            return null;
        }
    }

    private void setAltogamerScore(Game game) {
        float altoScore = 0;
        float count = 0;
        if (game.getScoreDestructoid() != null) {
            altoScore += game.getScoreDestructoid() * 10;
            count++;
        }
        if (game.getScoreGamespot() != null) {
            altoScore += game.getScoreGamespot() * 10;
            count++;
        }
        if (game.getScoreGamesradar() != null) {
            altoScore += game.getScoreGamesradar() * 10;
            count++;
        }
        if (game.getScoreIgn() != null) {
            altoScore += game.getScoreIgn() * 10;
            count++;
        }
        if (game.getScoreMetacritic() != null) {
            altoScore += game.getScoreMetacritic();
            count++;
        }
        if (game.getScorePcgamer() != null) {
            altoScore += game.getScorePcgamer();
            count++;
        }
        if (count >= 2) {
            altoScore = altoScore / count;
            game.setScoreAltogamer(Math.round(altoScore));
        } else {
            game.setScoreAltogamer(null);
        }
    }

}
