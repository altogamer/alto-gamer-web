/*  This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
altogamer.service.admin = (function() {

    var CHANGE_FEATURED_URL = altogamer.baseURI + "admin/api/game-change-featured?id=";
    var GET_GAME_URL = altogamer.baseURI + "admin/api/games/";
    var UPDATE_GAME_URL = altogamer.baseURI + "admin/api/games";
    var UPDATE_PUBLISHER_URL = altogamer.baseURI + "admin/api/publisher";
    var ADD_GAME_SYNONYM_URL = altogamer.baseURI + "admin/api/synonym";
    var GET_SYNONYM_BY_ALIAS_URL = altogamer.baseURI + "admin/api/synonym/alias/";
    var DELETE_SYNONYM_BY_ALIAS_URL = altogamer.baseURI + "admin/api/synonym/";

    function changeFeatured(idGame) {
        return $.getJSON(CHANGE_FEATURED_URL + idGame);
    }

    function findGameById(idGame) {
        return $.getJSON(GET_GAME_URL + idGame);
    }

    function updateGame(game) {
        return altogamer.service.putJSON({
            url: UPDATE_GAME_URL,
            data: game
        });
    }

    function updatePublisher(publisher) {
        return altogamer.service.putJSON({
            url: UPDATE_PUBLISHER_URL,
            data: publisher
        });
    }

    function addSynonym(gameSynonym) {
        return altogamer.service.postJSON({
            url: ADD_GAME_SYNONYM_URL,
            data: gameSynonym
        });
    }

    function deleteSynonym(id) {
        return altogamer.service.deleteJSON({
            url: DELETE_SYNONYM_BY_ALIAS_URL + id
        });
    }

    function findSynonymByAlias(alias) {
        return $.getJSON(GET_SYNONYM_BY_ALIAS_URL + alias);
    }


    return {
        changeFeatured: changeFeatured,
        updateGame: updateGame,
        updatePublisher: updatePublisher,
        addSynonym: addSynonym,
        findSynonymByAlias: findSynonymByAlias,
        deleteSynonym: deleteSynonym,
        findGameById: findGameById
    };

}());
