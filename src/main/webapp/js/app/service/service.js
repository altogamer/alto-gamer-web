/*  This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
altogamer.service = (function() {

    /** Wraps jQuery.ajax() to create a JSON POST request.
     * @param options an Object with ajax options. Valid options attributes:
     *   url: the URL for the post.
     *   data: the data to send, as an object. This object will be posted as JSON.
     *   success: a success callback function.
     *   error: an optional error callback function.
     *   complete: an optional callback to be called after success or error callbacks.
     */
    function postJSON(options) {
        return $.ajax({
            url: options.url,
            type: "POST",
            data: JSON.stringify(options.data),
            dataType: "json",
            contentType: "application/json"
        });
    }

    function putJSON(options) {
        return $.ajax({
            url: options.url,
            type: "PUT",
            data: JSON.stringify(options.data),
            dataType: "json",
            contentType: "application/json"
        });
    }

    function deleteJSON(options) {
        return $.ajax({
            url: options.url,
            type: "DELETE",
            data: JSON.stringify(options.data),
            dataType: "json",
            contentType: "application/json"
        });
    }

    return {
        deleteJSON: deleteJSON,
        postJSON: postJSON,
        putJSON: putJSON
    };
})();