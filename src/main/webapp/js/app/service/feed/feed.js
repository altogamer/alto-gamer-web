/*  This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
altogamer.service.feed = (function() {

    google.load("feeds", "1");

    function findByKeywords(query) {
        var dfd = $.Deferred();
        google.feeds.findFeeds(query, dfd.resolve);
        return dfd.promise();
    }

    return {
        findByKeywords: findByKeywords
    };

}());
