/*  This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var agPage = (function() {

    function init() {
        //adjustHighlightedGameHeight();

        //load featured game
        $(document).trigger("thumbAdded");
        $(document).trigger("scoreAdded");

        $("#gameList").altogamerInfiniteScrollList().on("agInfiniteScrollItemsRender", function() {
            $(document).trigger("thumbAdded");
            $(document).trigger("scoreAdded");
        });
    }

//    function adjustHighlightedGameHeight() {
//        var $data = $("#highlightedGameData");
//        var $cover = $("#highlightedGameCover");
//        $cover.height($data.height() - 20);
//    }

    return {
        init: init
    };
}());

$(document).ready(function() {
    agPage.init();
});
