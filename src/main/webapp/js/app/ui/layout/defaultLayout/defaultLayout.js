/*  This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
altogamer.ui.layout.defaultLayout = (function() {

    function init() {
        altogamer.ui.layout.defaultLayout.searchbox.init();
        altogamer.ui.layout.defaultLayout.navbar.init();
        altogamer.ui.layout.defaultLayout.scores.init();
        altogamer.ui.layout.defaultLayout.imageLoader.init();
        altogamer.ui.layout.defaultLayout.contextMenu.init();
    }

    return {
        init: init
    };

}());

altogamer.ui.layout.defaultLayout.scores = (function() {

    function init() {
        $(document).on("scoreAdded", prepareScores);
    }

    function prepareScores() {
        $(".ag-js-score-box").each(function(i, element) {
            var $scoreBox = $(element);
            var $scorePoints = $scoreBox.find(".ag-js-score-points");
            var scoreText = $scorePoints.data("score").toString();
            var score;
            if (scoreText.indexOf(".") !== -1) {
                score = parseFloat(scoreText);
                score = score * 10;
            }
            else {
                score = parseInt(scoreText);
            }

            if (score === 0) {
                $scoreBox.addClass("ag-score-zero");
            } else if (score < 50) {
                $scoreBox.addClass("ag-score-bad");
            }
            else if (score < 70) {
                $scoreBox.addClass("ag-score-regular");
            }
            else {
                $scoreBox.addClass("ag-score-good");
            }

            $scoreBox.removeClass("ag-js-score-box");
        });
    }

    return  {
        init: init
    };
})();

altogamer.ui.layout.defaultLayout.searchbox = (function() {

    function init() {
        var $searchTextInput = $("#ag-search-input");
        $searchTextInput.focus();
        $searchTextInput.altogamerGameSearchbox();
    }

    return {
        init: init
    };
})();

altogamer.ui.layout.defaultLayout.navbar = (function() {

    function init() {
        var $navbar = $("#navbar");
        var $body = $("body");
        var navbarHeight = $navbar.outerHeight() + "px";

        $navbar.affix({
            offset: {
                top: 156
            }
        });
        $navbar.on("affix.bs.affix", function() {
            $body.css("padding-top", navbarHeight);
        });
        $navbar.on("affix-top.bs.affix", function() {
            $body.css("padding-top", "0px");
        });
    }

    return {
        init: init
    };
})();


altogamer.ui.layout.defaultLayout.imageLoader = (function() {

    function init() {
        $(document).on("thumbAdded", loadImages);
    }

    function loadImages() {
        $(".js-ag-release-thumb").each(function(i, element) {
            var $item = $(element);
            var imageUrlCover = $item.data("imageUrlCover");
            loadImageInItem($item, imageUrlCover);
            $item.removeClass("js-ag-release-thumb");
        });
    }

    function loadImageInItem($item, imageUrl) {
        if (imageUrl) {
            altogamer.service.image.setImageAsBackground($item, imageUrl);
        }
        else {
            altogamer.service.image.findByKeywords({
                query: $item.data("searchKeywords"),
                size: "large",
                maxResults: 1
            }).done(function(data) {
                var result = data.responseData.results[0];
                if (result && result.url) {
                    altogamer.service.image.setImageAsBackground($item, result.url);
                }
                else {
                    $item.css("opacity", "1");
                }
            });
        }
    }

    return {
        init: init,
        loadImageInItem: loadImageInItem
    };
})();

altogamer.ui.layout.defaultLayout.contextMenu = (function() {

    var $currentItem;

    function init() {
        if (typeof agUser === "undefined") {
            return;
        }

        $("#editGameModal").on("submit", "#editGameForm", onSubmitEditGameForm);
        $("#addGameSynonymModal").on("submit", "#addSynonymForm", onSubmitAddSynonymForm);

        $(document).contextMenu({
            selector: ".js-ag-edit-context-menu",
            callback: function(menuKey) {
                $currentItem = $(this);
                var id = $currentItem.data("id");
                altogamer.service.admin.findGameById(id)
                        .done(function(game) {
                            switch (menuKey) {
                                case "edit":
                                    altogamer.ui.layout.defaultLayout.modal.showEditGameModal(game);
                                    break;
                                case "search":
                                    searchImages(game);
                                    break;
                                case "addSynonym":
                                    altogamer.ui.layout.defaultLayout.modal.showAddSynonymModal(game);
                                    break;
                            }
                        })
                        .error(function() {
                            alert("Error al cargar juego.");
                        });
            },
            items: {
                "edit": {name: "<span class='glyphicon glyphicon-pencil'></span> Editar imagen del juego"},
                "search": {name: "<span class='glyphicon glyphicon-search'></span> Buscar imágenes..."},
                "addSynonym": {name: "<span class='glyphicon glyphicon-pencil'></span> Agregar sinonimo"}
            }
        });
    }

    function searchImages(game) {
        var keywords = game.name + " game";
        var url = "https://www.google.com/search?as_st=y&tbm=isch&hl=en&as_q=" + encodeURIComponent(keywords) + "&as_epq=&as_oq=&as_eq=&cr=&as_sitesearch=&safe=images&tbs=isz:m#as_st=y&hl=en&q=" + encodeURIComponent(keywords) + "&tbm=isch&tbs=isz:m&imgdii=_";
        window.open(url);
    }

    function onSubmitEditGameForm() {
        var $form = $(this);
        var game = {
            id: $form.find("#editGameForm_id").val(),
            imageUrlCover: $form.find("#editGameForm_imageUrlCover").val()
        };

        altogamer.service.admin.updateGame(game)
                .success(function() {
                    $currentItem.data("imageUrlCover", game.imageUrlCover);
                    if (!$currentItem.hasClass("js-ag-no-image-cover")) {
                        altogamer.ui.layout.defaultLayout.imageLoader.loadImageInItem($currentItem, game.imageUrlCover);
                    }
                    altogamer.ui.layout.defaultLayout.modal.hideEditGameModal();
                })
                .error(function() {
                    alert("Ups, ocurrio un error guardando el juego...");
                });

        return false;
    }

    function onSubmitAddSynonymForm() {
        var $form = $(this);
        var gameSynonym = {
            alias: $form.find("#addSynonymForm_alias").val(),
            synonym: $form.find("#addSynonymForm_synonymAlias").val()
        }

        altogamer.service.admin.addSynonym(gameSynonym)
                .success(function() {
                    altogamer.ui.layout.defaultLayout.modal.hideAddSynonymModal();
                })
                .error(function() {
                    alert("Ocurrio un error al intentar agregar un sinonimo");
                });

        return false;
    }

    return {
        init: init
    };
})();

altogamer.ui.layout.defaultLayout.modal = (function() {

    function showEditGameModal(game) {
        $("#editGameModal").html($("#editGameModalTemplate").render(game));
        $("#editGameModal").modal("show");
        $("#editGameForm_imageUrlCover").click(function() {
            $(this).select();
        });

    }

    function hideEditGameModal() {
        $("#editGameModal").modal("hide");
    }

    function showAddSynonymModal(game) {
        $("#addGameSynonymModal").html($("#addGameSynonymModalTemplate").render(game));
        $("#addGameSynonymModal").modal("show");
    }

    function hideAddSynonymModal() {
        $("#addGameSynonymModal").modal("hide");
    }


    return {
        showEditGameModal: showEditGameModal,
        hideEditGameModal: hideEditGameModal,
        showAddSynonymModal: showAddSynonymModal,
        hideAddSynonymModal: hideAddSynonymModal
    };
})();

$(document).ready(function() {
    altogamer.ui.layout.defaultLayout.init();
});