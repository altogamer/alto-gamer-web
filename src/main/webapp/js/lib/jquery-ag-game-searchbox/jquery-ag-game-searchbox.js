/*  This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
(function($) {

    var cache = [];

    $.fn.altogamerGameSearchbox = function(options) {
        var settings = $.extend({
            minLength: 3
        }, options);

        this.autocomplete({
            source: searchGames,
            select: gameSelected,
            autoFocus: true,
            minLength: settings.minLength
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var releaseYear;
            if (item.releaseDate) {
                releaseYear = ", " + moment(item.releaseDate).year();
            }
            else {
                releaseYear = "";
            }
            return $("<li>")
                    .append("<a>" + item.name + " <small>(" + item.platform + releaseYear + ")</small></a>")
                    .appendTo(ul);
        };

        //force search if there is already some text
        if (this.val()) {
            this.autocomplete("search");
        }

        return this;
    };

    function searchGames(request, response) {
        var term = request.term;
        var pos = cache.indexOf(term);
        if (pos !== -1) {
            response(cache[pos]);
            return;
        }
        altogamer.service.game.findByKeywords(request.term).done(function(data) {
            cache[term] = data;
            response(data);
        });
    }

    function gameSelected(event, ui) {
        ui.item.value = ui.item.name; //"value" is used by autocomplete to render the selected value in the textfield
        altogamer.service.game.goTo(ui.item.platform, ui.item.alias);
    }

}(jQuery));