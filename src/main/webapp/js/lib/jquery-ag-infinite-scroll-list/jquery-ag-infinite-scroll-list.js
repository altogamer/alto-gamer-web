/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
(function($) {

    var EVENT_ITEMS_RENDER = "agInfiniteScrollItemsRender";
    var EVENT_EMPTY_RESULTS = "agInfiniteScrollEmptyResults";
    var DATA_NEXT_PAGE_URL = "agNextPageUrl";

    /**
     * Creates an infinite scrolling list.
     * The url to retrieve data must be set as attribute "data-ag-next-page-url".
     * This attribute will be updated with next next url to retrieve.
     *
     * The url must return a JSON response with 2 attributes:
     *   items: an Array with items to render.
     *   nextPageUrl: the url to retrieve the next page with data. This value
     *                will be stored in "data-ag-next-page-url".
     *
     * Events triggered by this container:
     *   agInfiniteScrollItemsRender: when new items are retrieved and rendered.
     *   agInfiniteScrollEmptyResults: when the url returns no data.
     */
    $.fn.altogamerInfiniteScrollList = function(options) {
        var $container = this;
        var settings = $.extend({
            scrollOffset: "125%",
            loadMoreItemsContainerId: "agLoadMoreItems",
            jsrenderTemplateId: "agInfiniteScrollListTemplate",
            hideLoadMoreItemsContainerOnEmptyResults: true
        }, options);

        prepareInfiniteScrolling($container, settings);

        return this;
    };

    function prepareInfiniteScrolling($container, settings) {
        var $waypoint = $("#" + settings.loadMoreItemsContainerId);
        $waypoint.waypoint(refreshItemsOnScroll, {offset: settings.scrollOffset});

        $container.on(EVENT_ITEMS_RENDER, function() {
            $waypoint.waypoint("destroy");
            $waypoint.waypoint(refreshItemsOnScroll, {offset: settings.scrollOffset});
        });
        $container.on(EVENT_EMPTY_RESULTS, function() {
            $waypoint.waypoint("destroy");
        });

        function refreshItemsOnScroll(direction) {
            var url;
            if (direction === "down") {
                url = $container.data(DATA_NEXT_PAGE_URL);
                renderUrl($container, settings, url);
            }
        }
    }

    function renderUrl($container, settings, url) {
        $.getJSON(url).success(function(result) {
            if (result.items && result.items.length > 0) {
                $container.append($("#" + settings.jsrenderTemplateId).render(result.items));
                $container.data(DATA_NEXT_PAGE_URL, result.nextPageUrl);
                $container.trigger(EVENT_ITEMS_RENDER);
            }
            else {
                $container.trigger(EVENT_EMPTY_RESULTS);
                if (settings.hideLoadMoreItemsContainerOnEmptyResults) {
                    $("#" + settings.loadMoreItemsContainerId).hide();
                }
            }
        });
    }

}(jQuery));