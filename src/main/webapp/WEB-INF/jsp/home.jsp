<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<div class="container">

    <c:if test="${not empty featured}">
        <div class="ag-featured">
            <c:set var="game" value="${featured[0]}" />
            <c:set var="gameUrl" value="/juegos/${game.platform}/${game.alias}"/>

            <div class="row">
                <div class="col-md-12">
                    <div class="ag-release">
                        <div class="ag-release-thumb-container">
                            <div class="js-ag-release-thumb js-ag-edit-context-menu ag-release-thumb ag-release-thumb-big" data-search-keywords="<c:out value="${game.name}"/> game" data-id="<c:out value="${game.id}"/>" data-image-url-cover="<c:out value="${game.imageUrlCover}"/>">
                                <div class="ag-featured-data">
                                    <div class="ag-game-header">
                                        <a href="<c:url value="${gameUrl}"/>">
                                            <h1>${game.name}</h1>
                                        </a>
                                        <span>${game.platform}</span>
                                        <c:if test="${not empty game.publisher}">
                                            <span class="glyphicon glyphicon-minus"></span>
                                            <span><a href="<c:url value="/"/>distribuidoras/${game.publisher}">${game.publisher}</a></span>
                                        </c:if>
                                        <c:if test="${not empty game.releaseDate}">
                                            <span class="glyphicon glyphicon-minus"></span>
                                            <span><fmt:formatDate type="date" dateStyle="long" value="${game.releaseDate}"/></span>
                                        </c:if>
                                    </div>

                                    <ul class="ag-review-list">
                                        <c:if test="${not empty game.scoreAltogamer}">
                                            <li class="ag-review-altogamer">
                                                <span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="${game.scoreAltogamer}">${game.scoreAltogamer}</span></span>
                                                Alto Gamer
                                            </li>
                                        </c:if>
                                        <c:if test="${not empty game.scoreDestructoid}">
                                            <li>
                                                <span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="${game.scoreDestructoid}">${game.scoreDestructoid}</span></span>
                                                <a href="${game.urlDestructoid}" target="_blank">Destructoid</a>
                                            </li>
                                        </c:if>
                                        <c:if test="${not empty game.scoreGamespot}">
                                            <li>
                                                <span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="${game.scoreGamespot}">${game.scoreGamespot}</span></span>
                                                <a href="${game.urlGamespot}" target="_blank">Gamespot</a>
                                            </li>
                                        </c:if>
                                        <c:if test="${not empty game.scoreGamesradar}">
                                            <li>
                                                <span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="${game.scoreGamesradar}">${game.scoreGamesradar}</span></span>
                                                <a href="${game.urlGamesradar}" target="_blank">Games Radar</a>
                                            </li>
                                        </c:if>
                                        <c:if test="${not empty game.scoreIgn}">
                                            <li>
                                                <span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="${game.scoreIgn}">${game.scoreIgn}</span></span>
                                                <a href="${game.urlIgn}" target="_blank">IGN</a>
                                            </li>
                                        </c:if>
                                        <c:if test="${not empty game.scoreMetacritic}">
                                            <li>
                                                <span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="${game.scoreMetacritic}">${game.scoreMetacritic}</span></span>
                                                <a href="${game.urlMetacritic}" target="_blank">Metacritic</a>
                                            </li>
                                        </c:if>
                                        <c:if test="${not empty game.scorePcgamer}">
                                            <li>
                                                <span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="${game.scorePcgamer}">${game.scorePcgamer}</span></span>
                                                <a href="${game.urlPcgamer}" target="_blank">PC Gamer</a>
                                            </li>
                                        </c:if>
                                        <c:if test="${not empty game.scoreVideogamer}">
                                            <li>
                                                <span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="${game.scoreVideogamer}">${game.scoreVideogamer}</span></span>
                                                <a href="${game.urlVideogamer}" target="_blank">Videogamer</a>
                                            </li>
                                        </c:if>
                                    </ul>

                                    <p class="ag-toolbar">
                                        <a class="hidden-xs hidden-sm ag-button cerulean" href="<c:url value="${gameUrl}"/>">Videos, capturas de pantalla y m�s!<span class="glyphicon glyphicon-chevron-right"></span></a>
                                        <a class="visible-xs visible-sm ag-button cerulean" href="<c:url value="${gameUrl}"/>">Ver m�s!<span class="glyphicon glyphicon-chevron-right"></span></a>
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </c:if>


    <div class="row">
        <div class="col-md-12">
            <div class="ag-title-section">
                <h2>Las novedades</h2>
                <h3>Los �ltimos juegos publicados</h3>
            </div>
        </div>
    </div>

    <div id="gameList" class="row" data-ag-next-page-url="<c:out value="${url}"/>"></div>

    <div class="row">
        <div class="col-md-12">
            <p id="agLoadMoreItems" class="ag-spinner">
                <span class="glyphicon glyphicon-refresh"></span> Cargando juegos...
            </p>
        </div>
    </div>

</div>

<%--********************************************************************
                               TEMPLATES
*********************************************************************--%>
<script id="agInfiniteScrollListTemplate" type="text/x-jsrender">
    <div class="col-md-4">
        <a class="ag-release" href="<c:url value="/"/>juegos/{{:platform}}/{{:alias}}">
            <div class="ag-release-thumb-container">
                <div class="js-ag-release-thumb js-ag-edit-context-menu ag-release-thumb" data-id="{{:id}}" data-search-keywords="{{>name}} game" data-image-url-cover="{{>imageUrlCover}}"></div>
                <div class="ag-review-list">
                    <span class="ag-score-box ag-score-box-news ag-js-score-box ag-data-score"><span class="ag-js-score-points" data-score="{{>scoreAltogamer}}">{{>scoreAltogamer}}</span></span>
                </div>
            </div>
            <h1>{{>name}}</h1>
        </a>
    </div>
</script>
