<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="ag-title-section ag-cerulean">
                <h2>Gestor de juegos</h2>
                <h3>Sin�nimos, destacados, listados y m�s</h3>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <br/>
            <form id="ag-manager-search-form" class="form-inline" role="form">
                <div class="form-group">
                    <input id="ag-manager-search-input" class="form-control" type="text" placeholder="Buscar juegos..." size="40">
                </div>
                <button type="submit" class="btn btn-default">Buscar</button>
            </form>

            <h3>Seleccionado</h3>
            <table class="table table-striped table-condensed">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Alias</th>
                        <th>Acci�n</th>
                    </tr>
                </thead>
                <tbody id="ag-manager-selected-game">
                    <tr>
                        <td>--</td>
                        <td>Click en un resultado para seleccionarlo</td>
                        <td>--</td>
                        <td>--</td>
                    </tr>
                </tbody>
                <tfoot id="ag-manager-selected-game-synonyms"></tfoot>
            </table>

            <h3>Resultados</h3>
            <table class="table table-hover table-condensed">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Alias</th>
                        <th>Acci�n</th>
                    </tr>
                </thead>
                <tbody id="ag-manager-games"></tbody>
            </table>
        </div>
    </div>
</div>

<script id="gameListTemplate" type="text/x-jsrender">
    <tr class="js-ag-select-game" data-game-index="{{:#index}}">
        <td>{{>id}}</td>
        <td>{{>name}}</td>
        <td>{{>alias}}</td>
        <td>
            <a target="_BLANK" href="<c:url value="/"/>juegos/{{:platform}}/{{:alias}}">Ver</a>
            -
            <a href="#" class="js-ag-synonym-add" data-alias="{{>alias}}">Sin�nimo</a>
        </td>
    </tr>
</script>

<script id="selectedGameTemplate" type="text/x-jsrender">
    <tr>
        <td>{{>id}}</td>
        <td>{{>name}}</td>
        <td>{{>alias}}</td>
        <td><a target="_BLANK" href="<c:url value="/"/>juegos/{{:platform}}/{{:alias}}">Ver</a></td>
    </tr>
</script>

<script id="selectedGameSynonymTemplate" type="text/x-jsrender">
    <tr>
        <td><span class="glyphicon glyphicon-chevron-right"></span></td>
        <td><small>&lt;sin�nimo&gt;</small></td>
        <td>{{>synonym}}</td>
        <td><a href="#" class="js-ag-synonym-delete" data-id="{{:id}}">Eliminar</a></td>
    </tr>
</script>