<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="ag-title-section">
                <h2>Login</h2>
                <h3>Bienvenido a Alto Gamer</h3>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <br/>
            <c:if test="${sessionScope.SPRING_SECURITY_LAST_EXCEPTION != null}" >
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Nombre de usuario o contraseņa incorrecta.</strong>
                </div>
                <c:remove var="SPRING_SECURITY_LAST_EXCEPTION" scope="session" />
            </c:if>
            <form class="form-horizontal" role="form" method="POST" action="<c:url value="/j_spring_security_check"/>">
                <input type="hidden" name="_spring_security_remember_me" value="on" />
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">Usuario</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="username" name="j_username" placeholder="Usuario">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-4 control-label">Contraseņa</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" id="password" name="j_password" placeholder="Contraseņa">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-primary">Entrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>