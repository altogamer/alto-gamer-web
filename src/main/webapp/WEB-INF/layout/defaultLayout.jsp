<%@page pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<!DOCTYPE html>
<html>
    <head>
        <%-- Variable that contains the current page URL --%>
        <c:set var="pageUrl" value="http://www.altogamer.com${requestScope['javax.servlet.forward.request_uri']}" scope="request"/>

        <title>${not empty game.name ? game.name.concat(" en Alto Gamer") : "Alto Gamer"}</title>

        <base href="<c:url value="/"/>"/>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">
        <meta name="keywords" content="juegos,review,games,pc,criticas,puntaje,videos,imagenes,capturas,screenshots,gameplay">
        <meta name="description" content="Alto Gamer es el sitio de gamers, para gamers. En un único lugar encontrás imágenes, videos y críticas de +10.000 juegos!">

        <%-- Facebook meta tags --%>
        <meta property="og:title" content="${not empty game.name ? game.name.concat(" en Alto Gamer") : "Alto Gamer"}" />
        <meta property="og:image" content="${not empty game.imageUrlCover ? game.imageUrlCover : "http://www.altogamer.com/img/banners/nocturno.png"}" />
        <meta property="og:description" content="${not empty game.name ? "Críticas de los mejores sitios de videojuegos, imágenes y videos de gameplay sobre ".concat(game.name).concat(". ") : ""}Alto Gamer es el sitio de gamers, para gamers. En un único lugar encontrás imágenes, videos y críticas de +10.000 juegos!" />
        <meta property="og:url" content="${pageUrl}" />

        <%-- Bing Webmaster Tools --%>
        <meta name="msvalidate.01" content="75961F28AF77632C2B288D6D92A352DE" />

        <link href="<c:url value="/img/favicon.ico"/>" rel="shortcut icon">

        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css">

        <link href="http://fonts.googleapis.com/css?family=Lobster" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,400italic" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz" rel="stylesheet" type="text/css">

        <link href="<c:url value="/css/jquery-ag-youtube.css"/>" rel="stylesheet" type="text/css">
        <link href="<c:url value="/css/jquery-colorbox.css"/>" rel="stylesheet" type="text/css">
        <link href="<c:url value="/css/jquery-contextMenu.css"/>" rel="stylesheet" type="text/css">
        <link href="<c:url value="/css/main.css"/>" rel="stylesheet" type="text/css">

        <%-- Google Analytics --%>
        <script>
            if (window.location.hostname.search("altogamer.com") !== -1) {
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                ga('create', 'UA-45271399-1', 'altogamer.com');
                ga('require', 'linkid', 'linkid.js');
                ga('send', 'pageview');
            }
        </script>
        <%-- End Google Analytics --%>
    </head>
    <body>
        <%-- Facebook library script. Read more: //read more: https://developers.facebook.com/docs/javascript/quickstart --%>
        <div id="fb-root"></div>
        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '730856466928330',
                    status     : false,
                    xfbml      : true
                });
            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/es_LA/all.js";
                fjs.parentNode.insertBefore(js, fjs);
             }(document, 'script', 'facebook-jssdk'));
        </script>
        <%-- End Facebook library script --%>

        <div class="ag-header">
            <div class="ag-logo"><a href="<c:url value="/"/>"><img src="<c:url value="/img/logo.png"/>" height="80" alt="Alto Gamer" /></a></div>
            <div class="ag-tagline">el sitio de gamers, para gamers</div>
        </div>


        <div id="navbar" class="ag-navbar-wrapper">
            <div class="ag-navbar">
                <ul>
                    <li><a href="<c:url value="/"/>">Inicio</a></li>
                    <li><span class="glyphicon glyphicon-star"></span></li>
                    <li><a href="<c:url value="/ranking"/>">Ranking</a></li>
                    <li><span class="glyphicon glyphicon-star"></span></li>
                    <li><a href="https://www.facebook.com/altogamerweb" target="_BLANK">Facebook</a></li>
                    <li><span class="glyphicon glyphicon-star"></span></li>
                    <li>
                        <div id="ag-search-form" class="ag-search-group">
                            <span class="ag-search-addon"><span class="glyphicon glyphicon-search"></span></span>
                            <input id="ag-search-input" class="ag-search-input" type="text" placeholder="Buscar juegos...">
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <tiles:insertAttribute name="body" />

        <div class="ag-footer ag-navbar">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 ag-copyleft">2014, Alto Gamer. <a href="mailto:contacto@altogamer.com">contacto@altogamer.com</a></div>
                    <div class="col-md-6 ag-menu">
                        <ul>
                            <li>Imágenes, videos y críticas de +10.000 juegos</li>
                            <li><a href="https://www.facebook.com/altogamerweb">Facebook</a></li>
                            <li><a href="https://twitter.com/AltoGamerWeb">Twitter</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <%--********************************************************************
                                       MODALS
        *********************************************************************--%>
        <sec:authorize access="hasRole('admin')">
            <div id="editGameModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="editGameModalLabel" aria-hidden="true"></div>
            <div id="addGameSynonymModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addGameSynonymModalLabel" aria-hidden="true"></div>

            <script id="editGameModalTemplate" type="text/x-jsrender">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3>Editar juego</h3>
                        </div>
                        <form id="editGameForm" class="form-horizontal">
                            <input type="hidden" id="editGameForm_id" name="id" value="{{:id}}"/>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Id</label>
                                    <div class="col-sm-9"><p class="form-control-static">{{:id}}</p></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Nombre</label>
                                    <div class="col-sm-9"><p class="form-control-static">{{>name}}</p></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Imagen</label>
                                    <div class="col-sm-9">
                                        <input type="url" id="editGameForm_imageUrlCover" name="imageUrlCover" class="form-control" placeholder="URL de imagen de cubierta..." value="{{>imageUrlCover}}" />
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-success" value="Guardar"/>
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </script>

            <script id="addGameSynonymModalTemplate" type="text/x-jsrender">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3>Agregar sin&oacute;nimo del juego</h3>
                        </div>
                        <form id="addSynonymForm" class="form-horizontal">
                            <input type="hidden" id="addSynonymForm_alias" name="alias" value="{{>alias}}"/>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Alias</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="addSynonymForm_synonymAlias" name="synonymAlias" class="form-control" placeholder="Alias del sinonimo" />
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-success" value="Guardar"/>
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </script>
        </sec:authorize>


        <%--********************************************************************
                                       SCRIPTS
        *********************************************************************--%>
        <sec:authorize access="hasRole('admin')">
            <script>
                var agUser = {username: '<sec:authentication property="principal.username" htmlEscape="false"/>'};
            </script>
        </sec:authorize>

        <!-- External libraries -->
        <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <script src="http://www.jsviews.com/download/jsrender.min.js"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script src="<c:url value="/js/lib/jquery-colorbox/jquery.colorbox-min.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-context-menu/jquery.contextMenu.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-waypoints/waypoints.min.js"/>"></script>
        <script src="<c:url value="/js/lib/moment/moment.min.js"/>"></script>

        <!-- Local libraries -->
        <script src="<c:url value="/js/lib/jquery-ag-game-searchbox/jquery-ag-game-searchbox.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-ag-infinite-scroll-list/jquery-ag-infinite-scroll-list.js"/>"></script>
        <script src="<c:url value="/js/lib/jquery-ag-youtube/jquery-ag-youtube.js"/>"></script>

        <!-- Alto Gamer libraries -->
        <script src="<c:url value="/js/app/altogamer.js"/>"></script>
        <script src="<c:url value="/js/app/service/service.js"/>"></script>
        <script src="<c:url value="/js/app/service/feed/feed.js"/>"></script>
        <script src="<c:url value="/js/app/service/game/game.js"/>"></script>
        <script src="<c:url value="/js/app/service/image/image.js"/>"></script>
        <script src="<c:url value="/js/app/ui/ui.js"/>"></script>
        <script src="<c:url value="/js/app/ui/layout/layout.js"/>"></script>
        <sec:authorize access="hasRole('admin')">
            <script src="<c:url value="/js/app/service/admin/admin.js"/>"></script>
        </sec:authorize>

        <!-- Layout init script -->
        <script src="<c:url value="/js/app/ui/layout/defaultLayout/defaultLayout.js"/>"></script>

        <!-- Page content init script -->
        <tiles:importAttribute name="pageJs" scope="page"/>
        <c:if test="${not empty pageJs}">
            <script src="<c:url value="${pageJs}"/>"></script>
        </c:if>
    </body>
</html>
